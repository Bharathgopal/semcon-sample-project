#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

#include "phone_book.h"

void test_add_contact(void) {
  contact_t new_contact = {"Add_user", "12345", "user1@email.com"};
  CU_ASSERT(add_contact(&new_contact) == 0);
}

void test_delete_contact(void) {
  contact_t new_contact = {"Delete_user", "12345", "user1@email.com"};
  CU_ASSERT(add_contact(&new_contact) == 0);
  CU_ASSERT(delete_contact(new_contact.name) == 0);
}

void test_modify_contact(void) {
  contact_t new_contact = {"Modify_User", "12345", "user1@email.com"};
  contact_t new_contact1 = {"Modified_User", "345", "new@email.com"};
  CU_ASSERT(add_contact(&new_contact) == 0);
  CU_ASSERT(modify_contact(new_contact.name, &new_contact1) == 0);
  CU_ASSERT(delete_contact(new_contact1.name) == 0);
}
void test_search_contact(void) {
  contact_t new_contact = {"Search_User", "345", "new@email.com"};
  CU_ASSERT(add_contact(&new_contact) == 0);
  CU_ASSERT(search_contact(new_contact.name) == 0);
  CU_ASSERT(delete_contact(new_contact.name) == 0);
}
void test_display_contact(void) { CU_ASSERT(display_contact() > 0); }
int main() {

  /*
      Initialize CUnit test registry
  */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /*
      Add suite to registry
  */
  CU_pSuite suite = CU_add_suite("Test Phone book", 0, 0);

  /*
      Add tests to the suite
  */
  CU_add_test(suite, "add contact", test_add_contact);
  CU_add_test(suite, "Display contact", test_display_contact);
  CU_add_test(suite, "delete contact", test_delete_contact);
  CU_add_test(suite, "Modify contact", test_modify_contact);
  CU_add_test(suite, "Search contact", test_search_contact);

  CU_basic_set_mode(CU_BRM_VERBOSE);
  /*
      Output to Screen
  */
  CU_basic_run_tests();
  /*
      Cleaning the Registry
  */
  CU_cleanup_registry();

  return 0;
}
