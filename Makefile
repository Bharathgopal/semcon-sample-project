ifeq (${OS},Windows_NT)

    MKDIR   := md
    RMDIR   := rd /S /Q

else

	UNAME_S := ${shell uname -s}

	ifeq (${UNAME_S},Linux)
        MKDIR   := mkdir -p
        RMDIR   := rm -rf
    endif

endif

PROJECT_NAME := "phone_book"
CC      := gcc
BUILD	:= ./build
BIN     := "${BUILD}"
OBJ     := "${BUILD}/obj"
INCLUDE := ./inc
SRC     := ./src
SRCS    := ${wildcard ${SRC}/*.c}
OBJS    := ${patsubst ${SRC}/%.c,${OBJ}/%.o,${SRCS}}
EXE     := "${BIN}/${PROJECT_NAME}.out"
CFLAGS  := -I${INCLUDE}
#LDLIBS  := -lm

.PHONY: all run clean test

all: ${EXE}

${EXE}: ${OBJ}/phone_book.o ${OBJS} | ${BIN}
	${CC} ${LD_LIBARAY_FLAGS} $^ -o $@ ${LDLIBS}

${OBJ}/phone_book.o: phone_book.c | ${OBJ}
	${CC} ${CFLAGS} -c $< -o $@

${OBJ}/%.o: ${SRC}/%.c | ${OBJ}
	${CC} ${CFLAGS} -c $< -o $@

${BIN} ${OBJ}:
	${MKDIR} $@

run: ${EXE}
	$<
test: 
	make -C test

clean:
	${RMDIR} ${OBJ} ${BIN}